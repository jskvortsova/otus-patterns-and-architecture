package ru.otus.patterns.multithreading.multiplier;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ru.otus.patterns.multithreading.model.Matrix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RunWith(Parameterized.class)
public class MatrixMultiplierTest {
    int threads;

    public MatrixMultiplierTest(int threads) {
        super();
        this.threads = threads;
    }

    @Parameterized.Parameters
    public static Collection input() {
        return Arrays.asList(1, 2, 3);
    }

    @Test
    public void testCalcValue() {

        int n = 2;
        Matrix matrixA = new Matrix(n);
        Matrix matrixB = new Matrix(n);
        Matrix matrixC = new Matrix(n);

        matrixA.setValue(0, 0, 2);
        matrixA.setValue(0, 1, 3);
        matrixA.setValue(1, 0, 3);
        matrixA.setValue(1, 1, 2);
        System.out.println("Matrix A: ");
        matrixA.print();

        matrixB.setValue(0, 0, 2);
        matrixB.setValue(0, 1, 3);
        matrixB.setValue(1, 0, 3);
        matrixB.setValue(1, 1, 2);
        System.out.println("Matrix B: ");
        matrixB.print();


        List<MatrixMultiplier> matrixMultipliers = new ArrayList<>();
        for (int i = 0; i < threads; i++) {
            MatrixMultiplier matrixMultiplier = new MatrixMultiplier(matrixA, matrixB, matrixC);
            matrixMultipliers.add(matrixMultiplier);
            matrixMultiplier.start();
        }
        for (MatrixMultiplier matrixMultiplier : matrixMultipliers) {
            try {
                matrixMultiplier.join();
            } catch (InterruptedException e) {
                matrixC.print();
            }
        }

        matrixC.print();

        Assert.assertTrue(13 == matrixC.getCell(0, 0).getValue());
        Assert.assertTrue(13 == matrixC.getCell(1, 1).getValue());
        Assert.assertTrue(12 == matrixC.getCell(1, 0).getValue());
        Assert.assertTrue(12 == matrixC.getCell(0, 1).getValue());
    }

}
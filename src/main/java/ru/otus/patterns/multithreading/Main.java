package ru.otus.patterns.multithreading;

import ru.otus.patterns.multithreading.model.Matrix;
import ru.otus.patterns.multithreading.multiplier.MatrixMultiplier;
import ru.otus.patterns.multithreading.reader.MatrixRandomize;
import ru.otus.patterns.multithreading.reader.MatrixReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        MatrixReader matrixConsoleReader = new MatrixRandomize();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер матрицы:");
        int n = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Введите элементы матрицы A через пробел:");
        Matrix matrixA = matrixConsoleReader.read(n);
        matrixA.print();
        System.out.println("Введите элементы матрицы B через пробел:");
        Matrix matrixB = matrixConsoleReader.read(n);
        matrixB.print();

        Matrix matrixC = new Matrix(n);

        System.out.println("Введите количество потоков:");
        int threads = scanner.nextInt();

        List<MatrixMultiplier> matrixMultipliers = new ArrayList<>();
        for (int i = 0; i < threads; i++) {
            MatrixMultiplier matrixMultiplier = new MatrixMultiplier(matrixA, matrixB, matrixC);
            matrixMultipliers.add(matrixMultiplier);
            matrixMultiplier.start();
        }
        for (MatrixMultiplier matrixMultiplier : matrixMultipliers) {
            try {
                matrixMultiplier.join();
            } catch (InterruptedException e) {
                matrixC.print();
            }
        }

        matrixC.print();

    }
}

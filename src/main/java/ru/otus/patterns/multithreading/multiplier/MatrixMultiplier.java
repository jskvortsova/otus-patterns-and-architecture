package ru.otus.patterns.multithreading.multiplier;

import ru.otus.patterns.multithreading.model.Matrix;

public class MatrixMultiplier extends Thread {
    private Matrix matrixA;
    private Matrix matrixB;
    private Matrix matrixC;


    public MatrixMultiplier(Matrix matrixA, Matrix matrixB, Matrix matrixC) {
        super();
        this.matrixA = matrixA;
        this.matrixB = matrixB;
        this.matrixC = matrixC;
    }

    @Override
    public void run() {
        int n = matrixC.getSize();
        int i = 0;
        int j = 0;
        while (i < n && j < n) {
            if (matrixC.getCell(i, j).isBusy() || matrixC.getCell(i, j).getValue() != null) {
                if (j < n - 1) {
                    j++;
                } else if (j == n - 1) {
                    j = 0;
                    i++;
                }
            } else {
                calcValue(i, j, n);
            }
        }

        System.out.println("Расчет окончен " + Thread.currentThread().getName());

    }

    private void calcValue(int i, int j, int k) {
        matrixC.getCell(i, j).setBusy(true);
        int sum = 0;
        for (int l = 0; l < k; l++) {
            sum += matrixA.getCell(i, l).getValue() * matrixB.getCell(l, j).getValue();
        }

        matrixC.setValue(i, j, sum);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            return;
        }
    }
}

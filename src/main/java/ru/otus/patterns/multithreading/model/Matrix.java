package ru.otus.patterns.multithreading.model;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class Matrix {
    Cell[][] values;
    int size;

    public Matrix(Cell[][] values, int size) {
        this.values = values;
        this.size = size;
    }

    public Matrix(int size) {
        this.size = size;
        this.values = new Cell[size][size];
    }

    public Cell getCell(int i, int j) {
        if(values[i][j] == null){
            values[i][j] = new Cell(null);
        }
        return values[i][j];
    }

    public void setValue(int i, int j, int v) {
        values[i][j] = new Cell(v);
    }

    public int getSize() {
        return size;
    }

    public void print(){
        String matrix = Arrays.asList(values).stream()
                .map(c -> Arrays.asList(c)
                        .stream()
                        .map(c1 -> c1.getValue().toString())
                        .collect(joining(" ")))
                .collect(joining("\n"));
        System.out.println(matrix);
    }

    public void clear(){
        this.values = new Cell[size][size];
    }

    public static class Cell {
        private boolean busy;
        private Integer value;

        public Cell(Integer value) {
            this.value = value;
            this.busy = false;
        }

        public boolean isBusy() {
            return busy;
        }

        public void setBusy(boolean busy) {
            this.busy = busy;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }
}

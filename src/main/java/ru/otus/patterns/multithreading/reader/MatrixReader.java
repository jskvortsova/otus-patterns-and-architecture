package ru.otus.patterns.multithreading.reader;

import ru.otus.patterns.multithreading.model.Matrix;

public interface MatrixReader {
    Matrix read(int n);
}

package ru.otus.patterns.multithreading.reader;

import ru.otus.patterns.multithreading.model.Matrix;

public class MatrixRandomize implements MatrixReader {
    @Override
    public Matrix read(int n) {
        Matrix matrix = new Matrix(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix.setValue(i, j, (int)(Math.random() * 10));
            }
        }
        return matrix;
    }
}

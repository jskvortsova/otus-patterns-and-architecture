package ru.otus.patterns.multithreading.reader;

import ru.otus.patterns.multithreading.model.Matrix;

import java.util.Scanner;


public class MatrixConsoleReader implements MatrixReader {

    @Override
    public Matrix read(int n) {
        Scanner scanner = new Scanner(System.in);
        Matrix.Cell[][] array = new Matrix.Cell[n][n];
        for (int i = 0; i < n; i++) {
            String l = scanner.nextLine();
            String[] line = l.split(" ");
            for (int j = 0; j < line.length; j++) {
                int value = Integer.parseInt(line[j]);
                array[i][j] = new Matrix.Cell(value);
            }
        }
        return new Matrix(array, n);
    }
}
